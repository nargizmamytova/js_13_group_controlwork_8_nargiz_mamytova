import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { Quotes } from '../shared/quotes.model';
import { map } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  allQuotes!: Quotes[];
   constructor(private route: ActivatedRoute, private http: HttpClient) { }

  ngOnInit(): void {

  }
}
