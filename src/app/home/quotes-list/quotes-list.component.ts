import { Component, OnInit } from '@angular/core';
import { Quotes } from '../../shared/quotes.model';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-quotes-list',
  templateUrl: './quotes-list.component.html',
  styleUrls: ['./quotes-list.component.css']
})
export class QuotesListComponent implements OnInit {
  allQuotes!: Quotes[];
  quotes!: Quotes;
  constructor(private http: HttpClient, private route: ActivatedRoute, private router: Router) {
  }

  ngOnInit(): void {
    this.route.params.subscribe((params: Params) => {
     if(params['id']){
       this.route.params.subscribe((params: Params) => {
         const quotesId = this.route.snapshot.params['id'];
         this.http.get<Quotes>(`https://plovo-bb6ec-default-rtdb.firebaseio.com/quotes/${quotesId}.json`)
           .pipe()
           .subscribe(results => {
             this.quotes = results;
             console.log(quotesId)
           })
       })

      }else {
       this.getPost()
      }
    })


  }
  getPost(){
  this.http.get<{ [id: string]: Quotes }>('https://plovo-bb6ec-default-rtdb.firebaseio.com/quotes.json')
    .pipe(map(result => {
      if (result === null) {
        return [];
      }
      return Object.keys(result).map(id => {
        const quotesValue = result[id];
        return new Quotes(
          id,
          quotesValue.text,
          quotesValue.author,
          quotesValue.category
        )
      });
    }))
    .subscribe(quotes => {
      this.allQuotes = quotes;
    })
}
  onDelete(id: string) {
    this.http.delete(`https://plovo-bb6ec-default-rtdb.firebaseio.com/quotes/${id}.json`).subscribe()
    this.http.get('https://plovo-bb6ec-default-rtdb.firebaseio.com/quotes.json').subscribe()
    void this.router.navigate(['']);
  }
}
