import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { QuotesListComponent } from './home/quotes-list/quotes-list.component';
import { SubmitNewComponent } from './submit-new/submit-new.component';

const routes: Routes = [
  {path: 'add-quotes', component: SubmitNewComponent},
  {path: 'quotes', component: QuotesListComponent},
  {path: '', component:  HomeComponent, children:[
  {path: ':id',component: SubmitNewComponent, children:[
      {path: 'edit', component: SubmitNewComponent}
    ]},



    ]},

  // {path: '**', component: NotFounds}


];


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
