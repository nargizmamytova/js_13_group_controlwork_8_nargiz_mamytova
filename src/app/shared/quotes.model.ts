export class Quotes{
  constructor(
    public id: string,
    public text: string,
    public author: string,
    public category: string,
  ) {}

}
