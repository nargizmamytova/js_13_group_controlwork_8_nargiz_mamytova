import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Quotes } from '../shared/quotes.model';

@Component({
  selector: 'app-submit-new',
  templateUrl: './submit-new.component.html',
  styleUrls: ['./submit-new.component.css']
})
export class SubmitNewComponent implements OnInit {
  author!: string;
  quotesText!: string;
  quotesId: number|null = null;
  @ViewChild('category')category!: ElementRef;

  constructor( private http: HttpClient, private router: Router, private route: ActivatedRoute) {

  }

  ngOnInit(): void {
    // this.route.params.subscribe((params:Params) =>{
    // if(params['id']){
//   const quotesId = this.route.snapshot.params['id'];
//   this.http.get<Quotes>(`https://plovo-bb6ec-default-rtdb.firebaseio.com/posts/${quotesId}.json`)
//     .pipe()
//     .subscribe(results => {
//       results.author = this.quotes.author,
//         results.text = this.quotes.text
//       results.category = this.quotes.category
//     })
//   const author = this.quotes.author;
//   const category = this.quotes.category;
//   const text = this.quotes.text;
//   let body = {author, category, text };
//   this.http.put<Quotes>
//   (`https://plovo-bb6ec-default-rtdb.firebaseio.com/quotes/${quotesId}.json`,body)
//     .pipe()
//     .subscribe()
// }
    // })
  }
  postQuotes(){
  const author = this.author;
  const newCategory = this.category.nativeElement.value;
  const category = newCategory;
  const text = this.quotesText;
  let body = {author, category, text };
  this.http.post('https://plovo-bb6ec-default-rtdb.firebaseio.com/quotes.json', body)
    .subscribe();
}
  onSave() {
    this.postQuotes()
    this.http.get('https://plovo-bb6ec-default-rtdb.firebaseio.com/quotes.json').subscribe()
    void this.router.navigate(['/']);
  }
}
